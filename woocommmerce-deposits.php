<?php
/*
 * Plugin Name: WC Deposit based discounts
 * Plugin URI: https://codup.io/
 * Description: Mark items as deposit items which customers can then place deposits on, rather than paying in full.
 * Version: 1.1.1.1
 * Author: Codup
 * Author URI: https://codup.io/
 * Text Domain: woocommerce-deposits
 * Domain Path: /languages
 */
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

if ( is_woocommerce_active() ) {
	/**
	 * WC_Deposits class
	 */
	class WC_Deposits {

		/** @var object Class Instance */
		private static $instance;

		/**
		 * Get the class instance
		 */
		public static function get_instance() {
			return null === self::$instance ? ( self::$instance = new self ) : self::$instance;
		}

		/**
		 * Constructor
		 */
		public function __construct() {
			global $wpdb;

			define( 'WC_DEPOSITS_VERSION', '1.1.8' );
			define( 'WC_DEPOSITS_FILE', __FILE__ );
			define( 'WC_DEPOSITS_PLUGIN_URL', untrailingslashit( plugins_url( basename( plugin_dir_path( __FILE__ ) ), basename( __FILE__ ) ) ) );
			define( 'WC_DEPOSITS_TEMPLATE_PATH', untrailingslashit( plugin_dir_path( __FILE__ ) ) . '/templates/' );

			register_activation_hook( __FILE__, array( $this, 'install' ) );

			if ( get_option( 'wc_deposits_version' ) !== WC_DEPOSITS_VERSION ) {
				add_action( 'shutdown', array( $this, 'delayed_install' ) );
			}

			$wpdb->wc_deposits_payment_plans          = $wpdb->prefix . 'wc_deposits_payment_plans';
			$wpdb->wc_deposits_payment_plans_schedule = $wpdb->prefix . 'wc_deposits_payment_plans_schedule';

			add_action( 'init', array( $this, 'load_plugin_textdomain' ) );
			add_action( 'plugins_loaded', array( $this, 'includes' ) );			
		}

		/**
		 * Localisation
		 */
		public function load_plugin_textdomain() {
			$locale = apply_filters( 'plugin_locale', get_locale(), 'woocommerce-deposits' );
			$dir    = trailingslashit( WP_LANG_DIR );
			load_textdomain( 'woocommerce-deposits', $dir . 'woocommerce-deposits/woocommerce-deposits-' . $locale . '.mo' );
			load_plugin_textdomain( 'woocommerce-deposits', false, dirname( plugin_basename( __FILE__ ) ) . '/languages/' );
		}

		/**
		 * Includes
		 */
		public function includes() {
			if ( is_admin() ) {
				include_once( 'includes/class-wc-deposits-settings.php' );
				include_once( 'includes/class-wc-deposits-plans-admin.php' );
				include_once( 'includes/class-wc-deposits-product-admin.php' );
			}
			include_once( 'includes/class-wc-deposits-plans-manager.php' );
			include_once( 'includes/class-wc-deposits-cart-manager.php' );
			include_once( 'includes/class-wc-deposits-order-manager.php' );
			include_once( 'includes/class-wc-deposits-order-item-manager.php' );
			include_once( 'includes/class-wc-deposits-scheduled-order-manager.php' );
			include_once( 'includes/class-wc-deposits-product-manager.php' );
			include_once( 'includes/class-wc-deposits-plan.php' );
		}		

		/**
		 * Installer
		 */
		public function install() {
			add_action( 'shutdown', array( $this, 'delayed_install' ) );
		}

		/**
		 * Installer (delayed)
		 */
		public function delayed_install() {
			global $wpdb, $wp_roles;

			$wpdb->hide_errors();

			$collate = '';

			if ( $wpdb->has_cap( 'collation' ) ) {
				if ( ! empty( $wpdb->charset ) ) {
					$collate .= "DEFAULT CHARACTER SET $wpdb->charset";
				}
				if ( ! empty( $wpdb->collate ) ) {
					$collate .= " COLLATE $wpdb->collate";
				}
			}

			require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );

			dbDelta( "
	CREATE TABLE {$wpdb->wc_deposits_payment_plans} (
	ID bigint(20) unsigned NOT NULL auto_increment,
	name varchar(255) NOT NULL,
	description longtext NOT NULL,
	PRIMARY KEY  (ID)
	) $collate;
	CREATE TABLE {$wpdb->wc_deposits_payment_plans_schedule} (
	schedule_id bigint(20) unsigned NOT NULL auto_increment,
	schedule_index bigint(20) unsigned NOT NULL default 0,
	plan_id bigint(20) unsigned NOT NULL,
	amount varchar(255) NOT NULL,
	interval_amount varchar(255) NOT NULL,
	interval_unit varchar(255) NOT NULL,
	PRIMARY KEY  (schedule_id),
	KEY plan_id (plan_id)
	) $collate;
			" );

			// Cron
			wp_clear_scheduled_hook( 'woocommerce_invoice_scheduled_orders' );
			wp_schedule_event( time(), 'hourly', 'woocommerce_invoice_scheduled_orders' );

			// Update version
			update_option( 'wc_deposits_version', WC_DEPOSITS_VERSION );
		}
	}
	WC_Deposits::get_instance();
}
